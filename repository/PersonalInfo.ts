import client from "../db/MySqlClient.ts";

export default class PersonalInfo {
    public async retrievePersonalInfo(id:number) : Promise<any>  {
        const sql = "SELECT firstname, middlename, lastname, birth_date, created_at FROM personal_info WHERE id = ?";
        const result = await client.execute(`${sql}`,[id]);

        return new Promise((resolve) => {
            resolve(result.rows);
        });
    }
}
