import client from "../db/MySqlClient.ts";
import { CredentialInterface } from "../models/authentication/credentialInterface.ts";

export default class Account {
    public async checkCredentials(params:CredentialInterface) : Promise<any>  {
        const sql = "SELECT * FROM accounts WHERE username = ? AND password = ?";
        const result = await client.execute(`${sql}`,[params.username, params.password]);

        return new Promise((resolve) => {
            resolve(result.rows);
        });
    }
}
