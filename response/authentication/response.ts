export default class Response {
    data = {};
    accountStatus = 0;

    public constructor(information:any, accountStatus:number) {
        this.data = information;
        this.accountStatus = accountStatus;
    }

    // TODO: Change datetime to server time
    public async getResponse() {
        return {
            success: true,
            server_time: new Date(),
            response: {
                account_status: this.accountStatus,
                data: this.data
            }
        };
    }
}
