export default class ErrorResponse {
    data = {
        error_code: 0,
        error_message: ''
    };

    public constructor(information:any) {
        this.data = information;
    }

    // TODO: Change datetime to server time
    public async getResponse() {
        return {
            success: false,
            server_time: new Date(),
            error: {
                code: this.data.error_code,
                message: this.data.error_message
            }
        }
    }
}
