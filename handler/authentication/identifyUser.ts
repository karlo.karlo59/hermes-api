import Account from "../../repository/Account.ts";
import PersonalInfo from "../../repository/PersonalInfo.ts";
import Response from "../../response/authentication/response.ts";
import ErrorResponse from "../../response/authentication/errorResponse.ts";
import * as hasCredentials from "../../specification/authentication/hasCredentials.ts";
import * as authenticationConstants from "../../generics/authenticationReponseCode.ts";

// TODO: Add bcrypt hashing
export async function identifyUser ({ params, response }: { params: any; response: any }) {
    const accounts = new Account();
    const result = await accounts.checkCredentials(params)
    .catch(error => {
        console.error('getAllUsers Method. async getAccounts error:%o', error)
    });
    const hasCredential = await hasCredentials.isSatisfiedBy(result.length);
    let responseFormatter = {};

    if (hasCredential) {
        const personalInfo = new PersonalInfo();
        const loggedInUserInfo = await personalInfo.retrievePersonalInfo(result[0].personal_info_id);

        responseFormatter = await new Response(loggedInUserInfo[0], result[0].status).getResponse();
    } else {
        responseFormatter = await new ErrorResponse({
           error_code: authenticationConstants.ERROR_RESPONSE_CODE.USER_DOES_NOT_EXISTS.CODE,
           error_message: authenticationConstants.ERROR_RESPONSE_CODE.USER_DOES_NOT_EXISTS.MESSAGE
        }).getResponse();
    }

    response.body = responseFormatter;
}
