import { Router } from 'https://deno.land/x/oak/mod.ts';
import { identifyUser } from '../handler/authentication/identifyUser.ts';

const router = new Router();

router.get("/authenticate/:username/password/:password", identifyUser);

export default router
