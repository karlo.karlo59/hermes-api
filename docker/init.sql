CREATE DATABASE `rjph_metrics`;

USE `rjph_metrics`;

CREATE TABLE IF NOT EXISTS `personal_info` (
    `id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `firstname` VARCHAR(100) NOT NULL,
    `middlename` VARCHAR(100) NOT NULL,
    `lastname` VARCHAR(100) NOT NULL,
    `birth_date` DATETIME NOT NULL,
    `created_at` DATETIME NOT NULL DEFAULT NOW(),
    `updated_AT` DATETIME NOT NULL DEFAULT NOW() ON UPDATE NOW()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `accounts` (
    `id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `username` VARCHAR(100) NOT NULL,
    `password` VARCHAR(150) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `status` TINYINT(2) UNSIGNED NOT NULL COMMENT '1=Active, 2=Inactive, 3=Locked, 4=SoftDeleted',
    `account_type_id` TINYINT(2) UNSIGNED NOT NULL COMMENT '1=SuperAdmin, 2=Admin, 3=Member',
    `created_at` DATETIME NOT NULL DEFAULT NOW(),
    `updated_AT` DATETIME NOT NULL DEFAULT NOW() ON UPDATE NOW()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
