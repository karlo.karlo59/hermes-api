export const SUCCESS_RESPONSE_CODE = {
    VERIFIED_USER: {
        CODE: 100,
        MESSAGE: "The User is successfully verified!"
    },
    ACTIVE_USER: {
        CODE: 101,
        MESSAGE: "The User account is active"
    }
};

export const ERROR_RESPONSE_CODE = {
    USER_DOES_NOT_EXISTS: {
        CODE: 1,
        MESSAGE: "No User has matched in our records"
    },
    INACTIVE_USER: {
        CODE: 2,
        MESSAGE: "This account is inactive, please contact administrator"
    }
};
