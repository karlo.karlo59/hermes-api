# hermes-api

## Generics
All the generic files or doesn't have any meaning in real world

## Handler
This is where you put the logic of the endpoint

## Model
Interface for objects

## Repository
- Create a file based on the table name
- Name the file in PascalCase

## Response
Response formatter

## Routes
The routes or the URL and the function to be called

---
## Others
- Typescript classes should be written in PascalCase `export default class ResponseFormatter { }`
- All files should be named in camelCase except the files in the `/repository` folder
- Variables and methods should be written in camelCase


# Response Code
- Code 1~100 = Error Code
- Code 100~ = Success Code
