import { Application } from 'https://deno.land/x/oak/mod.ts'
import { oakCors } from "https://deno.land/x/cors/mod.ts";
import authenticationRouter from './routes/authentication.ts';

const app = new Application();

// TODO: Create dynamic environment variable
app.use(
    oakCors({
        origin: "http://localhost:8080"
    }),
);

app.use(authenticationRouter.routes());
app.use(authenticationRouter.allowedMethods());

app.use((ctx) => {
    ctx.response.body = "Welcome to deno-rest-api";
});

await app.listen({ port: 8000 });
